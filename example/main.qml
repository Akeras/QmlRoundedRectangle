import QtQuick
import QtQuick.Window
import QmlRoundedRectangle

Window {
    width: 460
    height: 460
    visible: true
    title: qsTr("Rounded Rectangle example")

    RoundedRectangle {
        x: 20
        y: 20
        width: 200
        height: 200
        color: "darkgrey"
        borderColor: "grey"
        borderWidth: 2
        radius: 50
        radiusTL: 0
        radiusBR: 0
        Text {
            anchors.centerIn: parent
            text: "color: \"darkgrey\"\n\
borderColor: \"grey\"\n\
borderWidth: 2\n\
radius: 50\n\
radiusTL: 0\n\
radiusBR: 0"
        }
    }
    RoundedRectangle {
        x: 240
        y: 20
        width: 200
        height: 200
        color: "yellow"
        borderColor: "orange"
        borderWidth: 10
        radius: 0
        radiusTR: 80
        Text {
            anchors.centerIn: parent
            text: "color: \"yellow\"\n\
borderColor: \"orange\"\n\
borderWidth: 10\n\
radius: 0\n\
radiusTR: 80"
        }
    }
    RoundedRectangle {
        x: 20
        y: 240
        width: 200
        height: 200
        color: "transparent"
        borderColor: "orange"
        borderWidth: 5
        radius: 20
        Text {
            anchors.centerIn: parent
            text: "color: \"transparent\"\n\
borderColor: \"orange\"\n\
borderWidth: 5\n\
radius: 20"
        }
    }
    RoundedRectangle {
        x: 240
        y: 240
        width: 200
        height: 200
        color: "lightgrey"
        borderWidth: 0
        radius: 0
        radiusTR: 20
        Text {
            anchors.centerIn: parent
            text: "color: \"lightgrey\"\n\
borderWidth: 0\n\
radius: 0\n\
radiusTR: 20"
        }
    }
}
