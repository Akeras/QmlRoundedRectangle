# Qml Rounded Rectangle

A distance field based implementation of rounded rectangle with independently controllable corner radii in Qml. The implementation is inspired by the algorith described [in this blog post.](https://mortoray.com/2015/06/05/quickly-drawing-a-rounded-rectangle-with-a-gl-shader/)

## Requirements
- Qt 6.2 or later
- Cmake based qt project

## Installation
- Standalone:
  - Check out the code
  - Run cmake with `BUILD_EXAMPLE` set `ON`
- Using it in another project:
  - Check out the code in your project as a subfolder
  - In the top-level cmake file
    - add `add_subdirectory(QmlRoundedRectangle)`
    - add `libQmlRoundedRectangle` to your `target_link_libraries`
  - Use it in either in Qml as per the example
  - or you can use `RoundedRectangleNode` class for direct rendering from C++

![Example App Screenshot](assets/example.png "Example App Screenshot")
